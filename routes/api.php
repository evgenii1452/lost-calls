<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Controllers\Api\v1\CallsController;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group([
    "prefix" => "v1",
    "namespace" => "Api\\v1",
    "middleware" => "auth:api"
], function () use ($router) {

    //Звонки
    $router->get('calls', [
        "as" => "calls.index",
        "uses" => "CallsController@index",
    ]);

    $router->post('calls', [
        "uses" => "CallsController@store",
        "as" => "calls.update",
    ]);

    $router->put('calls', [
        "uses" => "CallsController@updateStatus",
        "as" => "calls.status.update",
    ]);

    //Пользователи
    $router->post('users', [
        "uses" => "UserController@store",
        "as" => "users.store",
    ]);

    $router->get('users', [
        "uses" => "UserController@index",
        "as" => "users.index",
    ]);

    $router->put('users/{id}', [
        "uses" => "UserController@update",
        "as" => "users.update",
    ]);

    $router->delete('users/{id}', [
        "uses" => "UserController@destroy",
        "as" => "users.destroy",
    ]);
});

