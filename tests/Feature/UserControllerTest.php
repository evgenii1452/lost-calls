<?php


namespace Feature;


use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\Generators\UserGenerator;
use TestCase;

/**
 * @group Users
 */
class UserControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function test_userAdminCanGetAllUsers()
    {
        $userAdmin = UserGenerator::createUserAdmin();

        $this->actingAs($userAdmin)
            ->json("get", route("users.index"))
            ->shouldReturnJson();
    }

    public function test_userAdminCanCreateUser()
    {
        $userAdmin = UserGenerator::createUserAdmin();
        $customerData = UserGenerator::generateUserCustomerData();

        $this->actingAs($userAdmin)
            ->json("post", route("users.store"), $customerData)
            ->seeJsonContains(["message" => "User created successfully"])
            ->seeInDatabase('users', ['login' => $customerData["login"]]);
    }

    public function test_userAdminCannotDeleteOtherAdmin()
    {
        $userAdmin = UserGenerator::createUserAdmin("user1");
        $userAdmin2 = UserGenerator::createUserAdmin("user2");

        $this->actingAs($userAdmin)
            ->json("delete", route("users.destroy", ["id" => $userAdmin2->id]))
            ->seeStatusCode(403)
            ->seeInDatabase('users', ['login' => $userAdmin2->login]);
    }

    public function test_userAdminCanDeleteCustomer()
    {
        $userAdmin = UserGenerator::createUserAdmin("user1");
        $userCustomer = UserGenerator::createUserCustomer("user2");

        $this->seeInDatabase('users', ['login' => $userCustomer->login]);

        $this->actingAs($userAdmin)
            ->json("delete", route("users.destroy", ["id" => $userCustomer->id]))
            ->seeStatusCode(204)
            ->notSeeInDatabase('users', ['login' => $userCustomer->login]);
    }

    public function test_userAdminCannotUpdateOtherAdmin()
    {
        $userAdmin1 = UserGenerator::createUserAdmin("user1");
        $userAdmin2 = UserGenerator::createUserAdmin("user2");

        $name = "NewName";

        $this->notSeeInDatabase("users", ["name" => $name])
            ->actingAs($userAdmin1)
            ->json(
                "put",
                route("users.update", ["id" => $userAdmin2->id]),
                ['name' => $name]
            )->seeStatusCode(403)
            ->notSeeInDatabase("users", ["name" => $name]);

    }

    public function test_userCustomerCannotGetAllUsers()
    {
        $userCustomer = UserGenerator::createUserCustomer("user1");

        $this->actingAs($userCustomer)
            ->json("get", route("users.index"))
            ->seeStatusCode(403);
    }

    public function test_userCustomerCannotCreateUser()
    {
        $userCustomer = UserGenerator::createUserCustomer("user1");
        $customerData = UserGenerator::generateUserCustomerData("user2");

        $this->actingAs($userCustomer)
            ->json("post", route("users.store"), $customerData)
            ->seeStatusCode(403)
            ->notSeeInDatabase('users', ['name' => $customerData["name"]]);
    }

    public function test_userCustomerCannotGetUser()
    {
        $userCustomer = UserGenerator::createUserCustomer();

        $this->actingAs($userCustomer)
            ->json("post", route("users.index"))
            ->seeStatusCode(403);
    }

    public function test_userCustomerCannotDeleteOtherCustomer()
    {
        $userCustomer1 = UserGenerator::createUserCustomer("user1");
        $userCustomer2 = UserGenerator::createUserCustomer("user2");

        $this->seeInDatabase('users', ['login' => $userCustomer2->login]);

        $this->actingAs($userCustomer1)
            ->json("delete", route("users.destroy", ["id" => $userCustomer2->id]))
            ->seeStatusCode(403)
            ->seeInDatabase('users', ['login' => $userCustomer2->login]);
    }

    public function test_userCustomerCanSelfUpdate()
    {
        $userCustomer = UserGenerator::createUserCustomer("user1");

        $name = "New name";
        $this->notSeeInDatabase("users", ["name" => $name]);


        $this->actingAs($userCustomer)
            ->json(
                "put",
                route("users.update", ["id" => $userCustomer->id]),
                ['name' => $name]
            )->seeStatusCode(200)
            ->seeInDatabase("users", ["name" => $name]);

    }

    public function test_userCustomerCannotUpdateOtherCustomer()
    {
        $userCustomer = UserGenerator::createUserCustomer("user1");
        $userCustomer2 = UserGenerator::createUserCustomer("user2");

        $name = "NewName";

        $this->notSeeInDatabase("users", ["name" => $name])
            ->actingAs($userCustomer)
            ->json(
                "put",
                route("users.update", ["id" => $userCustomer2->id]),
                ['name' => $name]
            )->seeStatusCode(403)
            ->notSeeInDatabase("users", ["name" => $name]);

    }
}
