<?php


namespace Tests\Generators;


use App\Infrastructure\Users\Constants\UserRoles;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserGenerator
{
    /**
     * @param string|null $login
     * @param string|null $password
     * @param int|null $role
     * @return array
     */
    public static function generateUserData(
        ?string $login,
        ?string $password,
        ?int $role): array
    {
        $faker = \Faker\Factory::create();

        return [
            'name' => Str::random(mt_rand(5, 10)),
            'login' => $login ?? Str::random(mt_rand(5, 10)),
            'email' => $faker->email,
            'role' => $role,
            'password' => $password ?? 'admin',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];
    }

    public static function generateUserCustomerData(string $login = "customer"): array
    {
        return self::generateUserData(
            $login,
            "customer",
            UserRoles::CUSTOMER
        );
    }

    public static function generateUserAdminData(string $login = "admin"): array
    {
        return self::generateUserData(
            $login,
            "admin",
            UserRoles::ADMIN
        );
    }

    public static function createUserCustomer(
        string $login = 'customer',
        string $password = 'customer'
    ): User {
        $userData = self::generateUserData($login, $password, UserRoles::CUSTOMER);
        $userData['password'] = Hash::make($userData['password']);

        return User::create($userData);
    }

    public static function createUserAdmin($login = 'admin', $password = 'admin'): User
    {
        $userData = self::generateUserData($login, $password, UserRoles::ADMIN);
        $userData['password'] = Hash::make($userData['password']);

        return User::create($userData);
    }

}
