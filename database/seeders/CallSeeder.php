<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class CallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $calls = $this->generateCalls();

        app("db")->table("calls")->insert($calls);
    }

    private function generateCalls(int $count = 2000): array
    {
        $faker = Factory::create("RU_ru");

        $calls = [];

        for ($i = 0; $i < $count; $i++) {
            $calls[] = [
                "is_lost" => (bool)mt_rand(0, 1),
                "date_time" => $faker->dateTimeBetween("-10 days"),
                "finish_reason" => "Абонент разорвал соединение",
                "region_name" => $faker->city,
                "from_phone_number" => $faker->phoneNumber(),
                "to_phone_number" => $faker->phoneNumber(),
                "processed" => (bool)mt_rand(0, 1),
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ];
        }

        return $calls;
    }
}
