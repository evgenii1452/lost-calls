<?php

namespace Database\Seeders;

use App\Infrastructure\Users\Constants\UserRoles;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            "name" => "administrator",
            "login" => "admin",
            "email" => "admin@mail.ru",
            "password" => app("hash")->make("admin"),
            "role" => UserRoles::ADMIN,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ];

        app("db")->table("users")->insert($user);
    }
}
