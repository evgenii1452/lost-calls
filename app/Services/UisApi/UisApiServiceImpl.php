<?php


namespace App\Services\UisApi;


use App\Exceptions\UisApiException;
use App\Infrastructure\Calls\UisApiService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class UisApiServiceImpl implements UisApiService
{

    /**
     * @param string $date
     * @return mixed
     * @throws UisApiException
     */
    public function getCallsReportByDate(string $date)
    {

        $dataFrom = Carbon::createFromDate($date)->startOfDay()->format("Y-m-d H:i:s");
        $dateTill = Carbon::createFromDate($date)->endOfDay()->format("Y-m-d H:i:s");

        $body = [
            "jsonrpc" => "2.0",
            "id" => mt_rand(1, 1000),
            "method" => UisApiConstants::DATA_API_GET_CALLS_REPORT_RESOURCE,
            "params" => [
                "access_token" => config('uis.access_token'),
                "filter" => [
                    'filters' => [
                        [
                            "field" => "is_lost",
                            "operator" => "=",
                            "value" => true
                        ],
                        [
                            "field" => "direction",
                            "operator" => "=",
                            "value" => "in"
                        ],
                    ],
                    "condition" => "and"
                ],
                "date_from" => $dataFrom,
                "date_till" => $dateTill
            ]
        ];

        $request = Http::withBody(json_encode($body), 'application/json')
            ->post(UisApiConstants::UIS_DATA_API_HOST . '/' . UisApiConstants::DATA_API_VERSION_2);

        $response = $request->object();

        if (isset($response->error)) {
            throw new UisApiException($response->error->message, $response->error->code);
        }

        return $response->result;
    }
}
