<?php


namespace App\Services\UisApi;


use App\Infrastructure\Calls\UisApiService;
use Carbon\Carbon;
use Faker\Factory;

class FakeUisApiServiceImpl implements UisApiService
{

    public function getCallsReportByDate(string $date): array
    {
        $faker = Factory::create("RU_ru");
        $count = mt_rand(1, 5);
        $calls = [];

        $startDateTime = Carbon::createFromFormat("Y-m-d", $date)
            ->startOfDay()
            ->toDateTime();
        $endDateTime = Carbon::createFromFormat("Y-m-d", $date)
            ->endOfDay()
            ->toDateTime();

        for ($i = 0; $i < $count; $i++) {
            $call = [
                'id' => mt_rand(10000, 999999),
                'is_lost' => (bool)mt_rand(0, 1),
                'start_time' => $faker->dateTimeBetween($startDateTime, $endDateTime),
                'finish_reason' => "Абонент разорвал соединение",
                'cpn_region_name' => $faker->city,
                'contact_phone_number' => $faker->phoneNumber,
                'virtual_phone_number' => $faker->phoneNumber,
            ];

            $calls[] = (object)$call;
        }

        return $calls;
    }
}
