<?php


namespace App\Services\UisApi;


class UisApiConstants
{
    /* Поставщики API */
    const UIS = 'UIS';

    const CALL_GEAR = 'CallGear';

    /* Названия API сервисов */
    const CALL_API_SERVICE = 'CallApi';

    const DATA_API_SERVICE = 'DataApi';

    /* URL адреса API сервисов UIS */
    const UIS_CALL_API_HOST = 'https://callapi.uiscom.ru';

    const UIS_DATA_API_HOST = 'https://dataapi.uiscom.ru';

    const CALL_GEAR_CALL_API_HOST = 'https://callapi.callgear.com';

    const CALL_GEAR_DATA_API_HOST = 'https://dataapi.callgear.com';

    /* Обозначение посетителя и оператора в терминологии UIS */
    const SITE_VISITOR = 'contact';

    const CLIENT_OPERATOR = 'operator';

    /* Варианты авторизации в API */
    const AUTH_BY_CREDENTIALS = 1;

    const AUTH_BY_API_KEY = 2;

    /* Версии API */
    const CALL_API_VERSION_4 = 'v4.0';

    const DATA_API_VERSION_2 = 'v2.0';

    /* Опции HTTP-запроса по умолчанию */
    const USER_AGENT = 'CBH-Uiscom-Client';

    /* Названия конечных методов API */
    const CALL_API_START_SIMPLE_CALL_RESOURCE = 'start.simple_call';

    const CALL_API_START_MULTI_CALL_RESOURCE = 'start.multi_call';

    const CALL_API_TAG_CALL_RESOURCE = 'tag.call';

    const DATA_API_GET_ACCOUNT_RESOURCE = 'get.account';

    const DATA_API_GET_CALLS_REPORT_RESOURCE = 'get.calls_report';


}
