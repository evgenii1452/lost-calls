<?php

namespace App\Providers;

use App\Infrastructure\Calls\Repositories\CallsRepositoryInterface;
use App\Infrastructure\Calls\Repositories\QBCallRepository;
use App\Infrastructure\Users\Repositories\EloquentCallsRepository;
use App\Infrastructure\Users\Repositories\EloquentUserRepository;
use App\Infrastructure\Users\Repositories\QBUserRepository;
use App\Infrastructure\Users\Repositories\UserRepositoryInterface;
use App\Services\UisApi\FakeUisApiServiceImpl;
use App\Infrastructure\Calls\UisApiService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CallsRepositoryInterface::class,
            EloquentCallsRepository::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            EloquentUserRepository::class
        );

        $this->app->bind(
            UisApiService::class,
            FakeUisApiServiceImpl::class
        );
    }
}
