<?php


namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function register()
    {
        \Dusterio\LumenPassport\LumenPassport::routes(
            $this->app,
            ['prefix' => 'api/oauth']
        );
    }
}
