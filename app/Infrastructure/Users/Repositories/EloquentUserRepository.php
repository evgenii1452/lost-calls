<?php


namespace App\Infrastructure\Users\Repositories;


use App\Infrastructure\Users\Repositories\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class EloquentUserRepository implements UserRepositoryInterface
{

    /**
     * @param array $data
     * @return Builder|Model|User
     */
    public function store(array $data): User
    {
        return User::query()->create($data);
    }

    /**
     * @return User[]|Collection
     */
    public function getAll(): Collection
    {
        return User::all();
    }

    /**
     * @param int $id
     * @return int
     */
    public function destroy(int $id): int
    {
        return User::destroy($id);
    }

    /**
     * @param int $id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function getById(int $id): ?User
    {
        return User::query()->find($id);
    }

    /**
     * @param int $id
     * @param array $data
     * @return int
     */
    public function updateById(int $id, array $data): int
    {
        return User::query()
            ->where('id', $id)
            ->update($data);
    }
}
