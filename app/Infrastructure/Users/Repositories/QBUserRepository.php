<?php


namespace App\Infrastructure\Users\Repositories;


use App\Infrastructure\Users\Repositories\UserRepositoryInterface;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

class QBUserRepository implements UserRepositoryInterface
{

    /**
     * @var Builder $db
     */
    private $qb;

    public function __construct()
    {
        $this->qb = app('db')->table("users");
    }

    public function store(array $data): bool
    {
        return $this->qb->insert($data);
    }

    public function getAll(): Collection
    {
        return $this->qb->get();
    }

    public function destroy(int $id): int
    {
        return $this->qb->delete($id);
    }

    public function getById(int $id): object
    {
        return $this->qb->find($id);
    }

    public function updateById(int $id, array $data): int
    {
        return $this->qb->find($id)->update($data);
    }
}
