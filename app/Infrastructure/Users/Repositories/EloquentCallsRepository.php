<?php


namespace App\Infrastructure\Users\Repositories;


use App\Models\Call;
use App\Infrastructure\Calls\Repositories\CallsRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;

class EloquentCallsRepository implements CallsRepositoryInterface
{

    public function createMany(array $data): int
    {
        return Call::query()->insertOrIgnore($data);
    }


    /**
     * @param $date
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getByDate(string $date): LengthAwarePaginator
    {
        return Call::query()
            ->whereDate('date_time', '=', $date)
            ->orderBy('date_time', 'desc')
            ->paginate(20);
    }

    /**
     * @param string $number
     * @param string $date
     * @param bool $status
     * @return int
     */
    public function changeStatusByNumberAndDate(string $number, string $date, bool $status): int
    {
        return Call::query()
            ->where('from_phone_number', $number)
            ->whereDate('date_time', '=', $date)
            ->update(['processed' => $status]);
    }
}
