<?php


namespace App\Infrastructure\Users\DTO;


use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UpdateUserDto
{
    private string $name;
    private int $role;
    private string $password;
    private string $updated_at;

    public function __construct()
    {
        $this->updated_at = Carbon::now();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        if (!is_null($name)) {
            $this->name = $name;
        }
    }

    /**
     * @return int
     */
    public function getRole(): int
    {
        return $this->role;
    }

    /**
     * @param int|null $role
     */
    public function setRole(?int $role): void
    {
        if (!is_null($role)) {
            $this->role = $role;
        }
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     */
    public function setPassword(?string $password): void
    {
        if (!is_null($password)) {
            $this->password = Hash::make($password);
        }
    }

    /**
     * @return Carbon|string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
