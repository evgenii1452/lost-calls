<?php


namespace App\Infrastructure\Users\DTO;


use App\Infrastructure\Users\Constants\UserRoles;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class CreateUserDto
{
    private string $name;
    private int $role;
    private string $password;
    private string $email;
    private string $login;
    private string $created_at;
    private string $updated_at;

    public function __construct(
        string $name,
        string $email,
        string $login,
        string $password,
        int $role = UserRoles::CUSTOMER
    ) {
        $this->name = $name;
        $this->role = $role;
        $this->password = Hash::make($password);
        $this->email = $email;
        $this->login = $login;
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getRole(): int
    {
        return $this->role;
    }

    /**
     * @param int $role
     */
    public function setRole(int $role): void
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = Hash::make($password);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
