<?php


namespace App\Infrastructure\Users;


use App\Exceptions\BusinessLogicException;
use App\Infrastructure\Users\DTO\CreateUserDto;
use App\Infrastructure\Users\DTO\UpdateUserDto;
use App\Models\User;
use App\Infrastructure\Users\Repositories\UserRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class UserService
{
    private UserRepositoryInterface $userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param CreateUserDto $createUserDto
     * @return void
     * @throws Exception
     */
    public function createUser(CreateUserDto $createUserDto): void
    {
        $created = $this->userRepository->store($createUserDto->toArray());

        if (!$created) {
            throw new Exception("Ошибка сохранения пользователя");
        }
    }


    /**
     * @return Collection
     */
    public function getAllUsers(): Collection
    {
        return $this->userRepository->getAll();
    }


    /**
     * @param int $id
     * @return int
     * @throws BusinessLogicException
     */
    public function deleteUser(int $id): int
    {
        $user = $this->getUserById($id);

        if ($user->isAdmin()) {
            throw new BusinessLogicException('Невозможно удалить админитратора', 403);
        }

        return $this->userRepository->destroy($id);
    }

    /**
     * @param int $id
     * @return User
     */
    public function getUserById(int $id): User
    {
        return $this->userRepository->getById($id);
    }

    /**
     * @param int $id
     * @param UpdateUserDto $user
     * @return int
     */
    public function updateUserById(int $id, UpdateUserDto $user): int
    {
        return $this->userRepository->updateById($id, $user->toArray());
    }
}
