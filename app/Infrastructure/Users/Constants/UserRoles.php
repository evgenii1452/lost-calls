<?php

namespace App\Infrastructure\Users\Constants;

class UserRoles
{
    const CUSTOMER = 0;
    const ADMIN = 1;
}
