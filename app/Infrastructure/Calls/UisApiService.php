<?php


namespace App\Infrastructure\Calls;


interface UisApiService
{
    public function getCallsReportByDate(string $date);
}
