<?php


namespace App\Infrastructure\Calls\Repositories;


use Illuminate\Pagination\LengthAwarePaginator;

interface CallsRepositoryInterface
{
    public function createMany(array $data): int;

    public function getByDate(string $date): LengthAwarePaginator;

    public function changeStatusByNumberAndDate(
        string $number,
        string $date,
        bool $status
    ): int;
}
