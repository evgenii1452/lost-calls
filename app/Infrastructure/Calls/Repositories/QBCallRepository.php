<?php


namespace App\Infrastructure\Calls\Repositories;

use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class QBCallRepository implements CallsRepositoryInterface
{

    /**
     * @var Builder
     */
    private $qb;

    public function __construct()
    {
        $this->qb = app("db")->table("calls");
    }

    public function createMany(array $data): int
    {
        return $this->qb->insertOrIgnore($data);
    }

    /**
     * @param string $date
     * @return LengthAwarePaginator
     */
    public function getByDate(string $date): LengthAwarePaginator
    {
        return $this->qb
            ->whereDate('date_time', '=', $date)
            ->orderBy('date_time', 'desc')
            ->paginate(20)->appends("date", $date);
    }

    public function changeStatusByNumberAndDate(
        string $number,
        string $date,
        bool $status
    ): int {
        return $this->qb
            ->where('from_phone_number', $number)
            ->whereDate('date_time', '=', $date)
            ->update(['processed' => $status]);
    }
}
