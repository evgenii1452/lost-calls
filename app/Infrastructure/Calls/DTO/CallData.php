<?php


namespace App\Infrastructure\Calls\DTO;


class CallData
{
    private int $id;
    private bool $isLost;
    private \DateTime $dateTime;
    private string $finishReason;
    private string $regionName;
    private string $fromPhoneNumber;
    private string $toPhoneNumber;

    public function __construct(
        int $id,
        bool $isLost,
        \DateTime $dateTime,
        string $finishReason,
        string $regionName,
        string $fromPhoneNumber,
        string $toPhoneNumber
    )
    {
        $this->id = $id;
        $this->isLost = $isLost;
        $this->dateTime = $dateTime;
        $this->finishReason = $finishReason;
        $this->regionName = $regionName;
        $this->fromPhoneNumber = $fromPhoneNumber;
        $this->toPhoneNumber = $toPhoneNumber;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function getIsLost(): bool
    {
        return $this->isLost;
    }

    /**
     * @return \DateTime
     */
    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }

    /**
     * @return string
     */
    public function getFinishReason(): string
    {
        return $this->finishReason;
    }

    /**
     * @return string
     */
    public function getRegionName(): string
    {
        return $this->regionName;
    }

    /**
     * @return string
     */
    public function getFromPhoneNumber(): string
    {
        return $this->fromPhoneNumber;
    }

    /**
     * @return string
     */
    public function getToPhoneNumber(): string
    {
        return $this->toPhoneNumber;
    }
}
