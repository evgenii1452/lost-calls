<?php


namespace App\Infrastructure\Calls;


use App\Exceptions\BusinessLogicException;
use App\Infrastructure\Calls\DTO\CallData;
use App\Infrastructure\Calls\Repositories\CallsRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

class CallsService
{
    private CallsRepositoryInterface $callsRepository;
    private UisApiService $uisApiService;

    public function __construct(
        CallsRepositoryInterface $callsRepository,
        UisApiService $uisApiService
    ) {
        $this->callsRepository = $callsRepository;
        $this->uisApiService = $uisApiService;
    }

    /**
     * @param CallData[] $callsData
     * @return int
     */
    public function storeFromCallsReport(array $callsData): int
    {
        $data = [];

        foreach ($callsData as $callData) {
            $data[] = [
                'id' => $callData->getId(),
                'is_lost' => $callData->getIsLost(),
                'date_time' => $callData->getDateTime(),
                'finish_reason' => $callData->getFinishReason(),
                'region_name' => $callData->getRegionName(),
                'from_phone_number' => $callData->getFromPhoneNumber(),
                'to_phone_number' => $callData->getToPhoneNumber(),
                'created_at' => Carbon::now(),
            ];
        }

        return $this->callsRepository->createMany($data);
    }

    /**
     * @param string $date
     * @return LengthAwarePaginator
     */
    public function getCallsByDate(string $date): LengthAwarePaginator
    {
        return $this->callsRepository->getByDate($date);
    }

    /**
     * @param string $date
     * @return void
     */
    public function requestCallsByDate(string $date): void
    {
        $callReports = $this->uisApiService->getCallsReportByDate($date);

        $callsData = [];

        foreach ($callReports as $callReport) {
            $callsData[] = new CallData(
                $callReport->id,
                $callReport->is_lost,
                $callReport->start_time,
                $callReport->finish_reason,
                $callReport->cpn_region_name,
                $callReport->contact_phone_number,
                $callReport->virtual_phone_number
            );
        }

        $this->storeFromCallsReport($callsData);
    }

    /**
     * @param string $number
     * @param string $date
     * @param bool $status
     * @return mixed
     */
    public function changeStatusCallsByNumberAndDate(
        string $number,
        string $date,
        bool $status
    ): void {
        $date = Carbon::createFromDate($date)->format("Y-m-d");

        $this->callsRepository->changeStatusByNumberAndDate($number, $date, $status);
    }
}
