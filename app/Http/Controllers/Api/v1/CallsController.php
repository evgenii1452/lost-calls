<?php

namespace App\Http\Controllers\Api\v1;

use App\Exceptions\BusinessLogicException;
use App\Http\Controllers\Controller;
use App\Infrastructure\Calls\CallsService;
use App\Infrastructure\Calls\UisApiService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;

class CallsController extends Controller
{
    private UisApiService $uisApiService;
    private CallsService $callsService;

    public function __construct(CallsService $callsService, UisApiService $uisApiService)
    {
        $this->uisApiService = $uisApiService;
        $this->callsService = $callsService;
    }


    /**
     * Поиск пропущенных звонков в базе по дате
     *
     * @param Request $request
     * @return Response|ResponseFactory
     * @throws BusinessLogicException
     */
    public function index(Request $request)
    {
        $this->validate($request, [
           "date" => "required|date_format:Y-m-d"
        ]);

        $date = $request->get("date");

        $callsPaginator = $this->callsService->getCallsByDate($date);

        return response($callsPaginator);
    }

    /**
     *  Запрос пропущенных звонков из апи и сохранение в базу
     *
     *
     * @param Request $request
     * @return Response|ResponseFactory
     * @throws BusinessLogicException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "date" => "required|date_format:Y-m-d"
        ]);

        $this->callsService->requestCallsByDate($request->get('date'));

        return response(["message" => "Records updated successfully"]);
    }

    /**
     * Изменения статуса звонка
     *
     * @param Request $request
     * @return Response|ResponseFactory
     */
    public function updateStatus(Request $request)
    {
        $this->callsService
            ->changeStatusCallsByNumberAndDate(
                $request->get('phoneNumber'),
                $request->get('date'),
                $request->get('processed')
            );

        return response(["message" => "Record updated successfully"]);
    }
}
