<?php


namespace App\Http\Controllers\Api\v1;


use App\Exceptions\BusinessLogicException;
use App\Http\Controllers\Controller;
use App\Infrastructure\Users\DTO\CreateUserDto;
use App\Infrastructure\Users\DTO\UpdateUserDto;
use App\Infrastructure\Users\UserService;
use App\Policies\Abilities;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Http\ResponseFactory;

class UserController extends Controller
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    /**
     * @return Response|ResponseFactory
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize(Abilities::INDEX, Auth::user());

        $users = $this->userService->getAllUsers();

        return response(['data' => $users]);
    }

    /**
     * @param int $id
     * @return Response|ResponseFactory
     * @throws BusinessLogicException|\Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $id)
    {
        $updatableUser = $this->userService->getUserById($id);
        $this->authorize(Abilities::DELETE, $updatableUser);

        $this->userService->deleteUser($id);

        return response('', 204);
    }

    /**
     * @param Request $request
     * @return Response|ResponseFactory
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize(Abilities::STORE, Auth::user());

        $createUserDto = new CreateUserDto(
            $request->get("name"),
            $request->get("email"),
            $request->get("login"),
            $request->get("password"),
            $request->get("role")
        );

        $this->userService->createUser($createUserDto);

        return response(["message" => "User created successfully"], 201);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response|ResponseFactory
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(int $id, Request $request)
    {
        $updatableUser = $this->userService->getUserById($id);
        $this->authorize(Abilities::UPDATE, $updatableUser);

        $updateUserDto = new UpdateUserDto();
        $updateUserDto->setName($request->get("name"));
        $updateUserDto->setRole($request->get("role"));
        $updateUserDto->setPassword($request->get("password"));

        $this->userService->updateUserById($id, $updateUserDto);

        return response(["message" => "User updated successfully"]);
    }

}
