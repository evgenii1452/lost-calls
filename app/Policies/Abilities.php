<?php


namespace App\Policies;



class Abilities
{
    const INDEX = 'index';
    const STORE = 'store';
    const UPDATE = 'update';
    const DELETE = 'delete';
}

