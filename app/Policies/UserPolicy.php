<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user)
    {
        return $user->isAdmin();
    }

    public function store(User $user)
    {
        return $user->isAdmin();
    }

    public function update(User $user, User $updatableUser)
    {
        if ($user->id === $updatableUser->id) {
            return true;
        }

        if ($user->isAdmin() && $updatableUser->isCustomer()) {
            return true;
        }

        return false;
    }

    public function delete(User $user, User $updatableUser)
    {
        if ($user->isAdmin() && $updatableUser->isCustomer()) {
            return true;
        }

        return false;
    }
}

