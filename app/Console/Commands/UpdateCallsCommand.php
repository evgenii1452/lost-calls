<?php


namespace App\Console\Commands;


use App\Infrastructure\Calls\CallsService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateCallsCommand extends Command
{
    protected $name = "updateCalls";

    public function handle(CallsService $callsService)
    {
        $date = Carbon::now()->format("Y-m-d");
        $callsService->requestCallsByDate($date);
    }
}
