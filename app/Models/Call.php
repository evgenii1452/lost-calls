<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'is_lost',
        'date_time',
        'finish_reason',
        'region_name',
        'from_phone_number',
        'to_phone_number',
        'status',
        'created_at',
        'updated_at',
    ];
}
